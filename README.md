# android_vendor_nintendo

This is a repository holding dumped firmware from Nintendo devices, including the Nintendo Switch (`nx`)

NOTE: ConsoleLauncher.apk obtained with special permission from K2 Apps

[App Listing](https://play.google.com/store/apps/details?id=com.k2.consolelauncher&hl=en_US&gl=US)
